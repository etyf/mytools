--[[ keys.lua ]]
local map = vim.api.nvim_set_keymap

-- remap the key used to leave insert mode
map('i', 'jk', '<esc>', {})
map('n', '<tab>', [[:NvimTreeToggle<CR>]], {silent=true})
map('n', '<F4>', [[:NvimTreeRefresh<CR>]], {silent=true})
map('n', '<F5>', [[:NvimTreeResize 30<CR>]], {silent=true})
map('n', '<F6>', [[:NvimTreeResize -2<CR>]], {silent=true})
map('n', '<F7>', [[:NvimTreeResize +2<CR>]], {silent=true})
