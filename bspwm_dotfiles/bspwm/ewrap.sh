#!/usr/bin/env sh

if [ -n "$TMUX" ] ; then
    # tmux session running
    tmux split-window -h "vim \"$*\""
else
    # Remove option --tab for new window
#   alacritty -o font.size=7 -e nvim -u ~/.vimrc $* &
    kitty -o font.size=7 -e nvim $* &
fi
