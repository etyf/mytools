#!/usr/bin/env bash
#packets: translate-shell xsel kdialog
a=`xsel -o | trans :ru -no-ansi -b`
echo -e "$a" > /tmp/kdetrans
kdialog 500 300 --title 'Перевод' --textbox /tmp/kdetrans
