#!/bin/bash
cp -r ~/.config/picom/ ../bspwm/picom/
cp -r ~/.config/bspwm/ ../bspwm/bspwm/
cp -r ~/.config/sxhkd/ ../bspwm/sxhkd/
cp -r ~/.config/kitty/ ../bspwm/kitty/
cp -r ~/.config/polybar/ ../bspwm/polybar/
cp ~/.bashrc bashrc
