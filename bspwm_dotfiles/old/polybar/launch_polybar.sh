if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    MONITOR=$m polybar --reload rightbar &
    MONITOR=$m polybar --reload leftbar &
  done
else
#  polybar --reload example &
   polybar --reload leftbar &
   polybar --reload rightbar &
fi
