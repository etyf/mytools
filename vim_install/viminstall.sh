#!/bin/bash
vimcheck=$(dpkg -s vim| grep -o '^Status: \w*'|awk '{ print $2 }')
if [ "$vimcheck" == "deinstall" ];then
  sudo apt install vim -y
#  echo "deinsttall"
#elif [ "$vimcheck" == "install" ];then
#  echo "install"
fi

viminstallpath="$(pwd)$(find . -name viminstall.sh|cut -c2-|rev|cut -d/ -f2-|rev)"
DATECUR=$(date -I|sed 's/-/_/g')
mv ~/.vim ~/.vim_$DATECUR > /dev/null 2>&1
mv ~/.vimrc ~/.vimrc_$DATECUR > /dev/null 2>&1
cp -r $viminstallpath/.vim/ ~/ > /dev/null 2>&1
cp -r $viminstallpath/.vimrc ~/ > /dev/null 2>&1
