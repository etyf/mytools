#!/bin/bash
tmux_check=$(dpkg -s tmux| grep -o '^Status: \w*'|awk '{ print $2 }')
if [ "$tmux_check" == "deinstall" ];then
  sudo apt install tmux -y
#  echo "deinsttall"
#elif [ "$vimcheck" == "install" ];then
#  echo "install"
fi

TMUXRUN="$(pwd)$(find . -name tmuxinstall.sh|cut -c2-|rev|cut -d/ -f2-|rev)"
echo "python3 $TMUXRUN/tmuxpy.py && tmux a" > ~/tmuxrun.sh
chmod +x ~/tmuxrun.sh
