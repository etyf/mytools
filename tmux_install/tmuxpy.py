#!/usr/bin/env python3
import subprocess
from subprocess import PIPE
import os
import time

# tmux settings
#session name, path, number of panes
sessions_config=[
    ["pythonscripts","~/dev/pythonscripts",4],
    ["vagrant","~/dev/vagrantscripts/VFILES",6],
    ["start","~/start/",4],
    ["userlist","~/start/userlist",4],
    ]

#tmux global settings
tmux_global="""
tmux set -g base-index 1
tmux set -g pane-base-index 1
tmux set -g mouse on
tmux set-option -g repeat-time 0
tmux unbind C-b
tmux unbind C-a
tmux set-option -g prefix C-a
tmux bind-key C-a send-prefix
tmux bind-key -r -T prefix w choose-tree -s
tmux set -g status-bg magenta
tmux set -g pane-active-border-style "bg=default fg=magenta"
"""
# MODULES
#execute linux command
def exec_command(cmd):
    cmd=cmd.split()
    p=subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out,err = p.communicate()
    return (p.returncode,out,err)

#create session
def create_sessions(arr):
    for i in arr:
        cmdstring="tmux new-session -d -s "+i
        os.system(cmdstring)
    #apply tmux settings
    glob=tmux_global.split("\n")[1:-1]
    for i in glob: os.system(i)
   
#create panes in unique sessions
def create_panes(arr):
    name=arr[0]
    path=arr[1]
    panes=arr[2]
    #first split window
    pane_left=panes//2
    pane_right=panes-pane_left
    cmdstring="tmux send-key -t "+name+" tmux\ split-window\ -t\ 1\ -h C-m C-l"
    os.system(cmdstring)
    #split right pane 
    for i in range(pane_right-1): os.system("tmux send-key -t "+name+" tmux\ split-window\ -t\ 2\ -v C-m C-l")
    #split left pane 
    for i in range(pane_left-1): os.system("tmux send-key -t "+name+" tmux\ split-window\ -t\ 1\ -v C-m C-l")
    #cd to path on unique panes
    time.sleep(0.1)
    panes_sum=subprocess.getoutput("tmux list-panes -t "+name+" |wc -l")
    for pane in range(int(panes_sum)): os.system("tmux send-key -t "+name+"."+str(pane+1)+" cd\ "+path+" C-m C-l")

#run server if not exists, create sessions
sessions_config_names=[i[0] for i in sessions_config]
if exec_command("tmux list-sessions")[0] == 1:
    sessions_unique=[i[0] for i in sessions_config]
    create_sessions(sessions_unique)
else:
    #get sessionslist. [:-1] cos last \n symbol
    sessions_list=exec_command("tmux list-sessions")[1].decode('utf-8')[:-1]
    #find created sessions names to array
    sessions_exist_names=[i.split()[0][:-1] for i in sessions_list.split('\n')] 
    #unique array from sessions_exist_names and sessions_config_names
    sessions_unique=[sessions_config_names[i] for i in range(len(sessions_config_names)) if sessions_config_names[i] not in sessions_exist_names]
    create_sessions(sessions_unique)

# edit panes for unique sessions
#make array with unique sessions settings
sessions_config_unique=[]
for unique in sessions_unique:
    for config in sessions_config:
        if unique == config[0]: sessions_config_unique+=[config]

#add panes to unique session
for i in sessions_config_unique:
   create_panes(i)

