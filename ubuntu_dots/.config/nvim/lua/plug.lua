return require('packer').startup(function(use)
  -- [[ Plugins Go Here ]]
--  use 'karb94/neoscroll.nvim'
  use 'psliwka/vim-smoothie'
  use 'wbthomason/packer.nvim'
  use {
    'kyazdani42/nvim-tree.lua',
    requires = {
      'kyazdani42/nvim-web-devicons', -- optional, for file icon
    },
    tag = 'nightly' -- optional, updated every week. (see issue #1193)
}
use({
  "nvim-treesitter/nvim-treesitter-textobjects",
  after = "nvim-treesitter",
  requires = "nvim-treesitter/nvim-treesitter",
})
  use {"lukas-reineke/indent-blankline.nvim"}
  use {"tamton-aquib/staline.nvim"}
  use { "catppuccin/nvim", as = "catppuccin" }
  use {"rebelot/kanagawa.nvim"}
end)
-- ,
-- config = {
--   package_root = vim.fn.stdpath('config') .. '/site/pack'
-- })

