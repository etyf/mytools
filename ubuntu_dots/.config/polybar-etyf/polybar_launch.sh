#!/usr/bin/env bash
polybar_theme="bottled"

for i in $(pgrep -u $UID polybar);do kill $i;done;sleep 1
#polybar -c polybar_theme/config.ini
theme_config_path=$(ls|grep $polybar_theme)"/config.ini"
#theme_config_path=$(ls|grep $polybar_theme)
polybar --reload core -c $theme_config_path &
