#!/usr/bin/env bash

THEME="lofi_edited"
killall polybar
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

CONFIG_DIR=$(dirname $0)/themes/$THEME/config.ini
polybar main -c $CONFIG_DIR &
polybar main -c /home/miklekonovalov/polybar-collection/themes/lofi_edited/config.ini &

#second
polybar main_external -c $CONFIG_DIR &
polybar main_external -c /home/miklekonovalov/polybar-collection/themes/lofi_edited/config.ini &
