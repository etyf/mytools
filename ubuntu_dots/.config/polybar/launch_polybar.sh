if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    MONITOR=$m polybar --reload rightbar &
    MONITOR=$m polybar --reload leftbar &
    MONITOR=$m polybar --reload centerbar &
    MONITOR=$m polybar --reload centerbar_under &
  done
else
#  polybar --reload example &
   polybar --reload leftbar &
   polybar --reload rightbar &
   polybar --reload centerbar &
   polybar --reload centerbar_under &
fi
