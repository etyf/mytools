#!/bin/bash
layouts=("us" "ru")
curent_layout=$(setxkbmap -query|grep layout|awk '{print $2}')
for i in ${layouts[@]};do if [ "$curent_layout" != "$i" ];then setxkbmap "$i";fi;done
setxkbmap -option grp:alt_shift_toggle ru,us &

