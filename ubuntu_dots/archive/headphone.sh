#!/bin/bash
check=$(bluetoothctl info 78:04:E3:9E:12:74|grep -i connected|awk '{print $2}')
if [[ $check = "no" ]]
then
  echo " -- starting to enable"
  bluetoothctl connect 78:04:E3:9E:12:74
else
  echo " -- starting to disable"
  bluetoothctl disconnect 78:04:E3:9E:12:74
fi
echo " -- done"
